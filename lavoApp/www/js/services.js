angular.module('lavoApp.services', [])
.service('appServices', function appServices($q) {
    // Wrap the barcode scanner in a service so that it can be shared easily.
    this.scanBarcode = function() {
        /*
        $cordovaBarcodeScanner.scan().then(function(imageData) {
            alert(imageData.text);
            console.log("Barcode Format -> " + imageData.format);
            console.log("Cancelled -> " + imageData.cancelled);
        }, function(error) {
            console.log("An error happened -> " + error);
        });

        // The plugin operates asynchronously so a promise
        // must be used to display the results correctly.
        var deferred = $q.defer();
        try {
            cordova.plugins.barcodeScanner.scan(
                function (result) {  // success
                    deferred.resolve({'error':false, 'result': result});
                }, 
                function (error) {  // failure
                    deferred.resolve({'error':true, 'result': error.toString()});
                }
            );
        }
        catch (exc) {
            deferred.resolve({'error':true, 'result': 'exception: ' + exc.toString()});
        }
        return deferred.promise;*/
    };
})
.factory('Request', ["$http","$location","$rootScope", /*"Auth",*/"$timeout",function($http,$location,$rootScope,Auth,$timeout){
    
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    
    return {
        get: function(option){

            var validate_response = function(data) {

                if(data.error && data.message) {

                    if(!response.success) {
                        
                        $scope.show_message(data.message,true);  
                        /*var alertPopup = $ionicPopup.alert({
                            title: data.title,
                            template: data.description
                        });
                        alertPopup.then(function(res) {
                            //console.log('Thank you for not eating my delicious ice cream cone');
                        });*/
                        return;
                    }
                }
            };
            
            return $http(option).success(validate_response)
            .error(validate_response);
        }
    }
}])
.factory('lavoAPI',['Request',/*'Auth',*/'$location','$rootScope','$timeout',function(Request,Auth,$location,$rootScope,$timeout) {
    // $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    
    var defaults = {
        token: null,
        status: false,
        profile: null
    },

    // API urls
    //api_url = 'http://agilelogix.com/develope/';    // Staging
    api_url = 'http://45.55.27.248/api/';    // Production
    //api_url = 'http://lavo.localhost.com/api/';
	//api_url = 'http://192.168.10.4/scannerServer/public/api/';


    // If there's Cookie, set default to it
    if(Cookies.get('token')) defaults.token = Cookies.get('token'); 

    // ---
    // PRVIATE METHODS.
    // ---

    var lavoAPI = {};


    //ugly hack b/c of Angular Bug
    var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
          value = obj[name];

          if(value instanceof Array) {
            for(i=0; i<value.length; ++i) {
              subValue = value[i];
              fullSubName = name + '[' + i + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value instanceof Object) {
            for(subName in value) {
              subValue = value[subName];
              fullSubName = name + '[' + subName + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value !== undefined && value !== null)
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    var transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];

    
    lavoAPI.getURL = function(){
        return api_url;
    };    

    // API request of login
    lavoAPI.login = function(_data) {

        return Request.get({
            method    : 'POST',
            transformRequest: transformRequest,
            url: api_url +'authenticate',
            data: _data
        });
    };



    lavoAPI.setToken = function(_newToken,_status) {
        defaults.token = _newToken;
        defaults.status = _status;
    };

    lavoAPI.getToken = function(){
        return defaults.token ;
    };


    // API GET
    lavoAPI.get = function(_url,_withoutToken) {
        var URL = (!_withoutToken)?api_url +_url + '?token='+defaults.token:api_url +_url;
        return Request.get({
            method: 'GET', 
            url: URL
        });
    };


    // API post
    lavoAPI.post = function(_url, _data) {
        
        //var _data = JSON.stringify(_data),
        var _data = (_data),
            _url = api_url +_url +  '?token='+defaults.token;

        return Request.get({
            method:     'POST',
            url:        _url,
            data:       _data,
            transformRequest: transformRequest
        });
    }


    // API delete
    lavoAPI.delete = function(_url) {
        var _url = api_url +_url +  '?token='+defaults.token;

        return Request.get({
            method:     'DELETE',
            url:        _url,
            headers:    {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    }

    return lavoAPI;
}])
// Auth & User Profile 
.factory('Auth', function($rootScope) {

    var user,profile = null,
        login_status = false;

    var slug_url = 'lavo.localhost.com';
    
    //$.ajaxSetup({headers: {'X-SOURCE-HOST': slug_url}});

    return {
        setUser: function(aUser){
            console.log(aUser);
            user = aUser;
            login_status = true;
            // Set avatar cookie
            Cookies.set('avatar', (aUser.avatar || '/img/default/avatar.jpg'));
        },
        getUser: function(){
            return user;
        },
        setStatus: function(_status){
            login_status = _status;
        },
        getId: function(){
            return user.user.id;
        },
        getSlug: function(){
            return slug_name;
        },
        getSlugURL: function(){
            return slug_url;
        },
        destroy: function() {
            user = null;
            login_status = false;
            Cookies.remove('token');//{ path: '/' }
        },
        destroyAll: function() {
            user = null;
            login_status = false;
            Cookies.remove('token');
            Cookies.remove('email');
            Cookies.remove('avatar');
        },
        isLoggedIn : function(){
            return login_status;
        },
        createSession: function(token,data){
            // Setup cookie
            Cookies.set("token", token);
            if(data.email)Cookies.set("email", data.email);
        },
        getSession: function(){
            if(Cookies.get("email") && Cookies.get("avatar")) {
                return {
                    e: Cookies.get("email"),
                    a: Cookies.get("avatar")
                }
            }
            return null;
        }
    }
})
