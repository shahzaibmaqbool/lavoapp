// var appControllers = angular.module('starter.controllers', []); // Use for all controller of application.
// var appServices = angular.module('starter.services', []);// Use for all service of application.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////lavo App controller code///////////////////////////////////////////////////////////////////////

angular.module('lavoApp.controllers', [])
.controller('ScanCtrl', function($scope, appServices,$cordovaBarcodeScanner,lavoAPI,$rootScope,$ionicPopup) {
    
    $scope.message = '';

    $scope.properties = [
        { title: 'Sodium' },
        { title: 'Fiber' },
        { title: 'Sugar' },
        { title: 'Protien' },
        { title: 'Fat' },
        { title: 'Vitamin' },
        { title: 'Minerals' },
        { title: 'Bad' }
    ];

    /*UI Variables*/


    $scope.loading = false;

    //A Dummy Method
    $scope.get_product = function(){
        
        var upc = '041500763590';
        //Get the Response
        lavoAPI.get('product/'+upc).success(function (response) {
            

            if(!response.success) {
                
                var alertPopup = $ionicPopup.alert({
                    title: 'Error!',
                    template: 'Product Not Found'
                });
                alertPopup.then(function(res) {
                    //console.log('Thank you for not eating my delicious ice cream cone');
                });
                return;
            }

            $rootScope.product  = response.data;
            $rootScope.full_rating  = response.data.calories;
            $rootScope.food_name    = response.data.name;
            $rootScope.category     = 'test';
            $rootScope.fiber_rank = _.random(0,10);
            $rootScope.sugar_rank = _.random(0,10);

            $rootScope.alternate = response.alternate;
            
            $rootScope.nutrient  = _.pairs(response.nutrient);
            $rootScope.vitamin   = _.pairs(response.vitamin);
        });
    }

    //Method to Scan Image
    $scope.scanImage = function() {

        /*$scope.get_product();
        return ;*/
        $cordovaBarcodeScanner.scan().then(function(imageData) {
            
            $scope.loading = true;
            
            //Code Found
            if(imageData.text) {

                //Get the Response
                lavoAPI.get('product/'+imageData.text).success(function (response) {
                    
                    if(!response.success) {
                        
                        var alertPopup = $ionicPopup.alert({
                            title: 'Error!',
                            template: 'Product Not Found'
                        });
                        alertPopup.then(function(res) {
                            //console.log('Thank you for not eating my delicious ice cream cone');
                        });
                        return;
                    }

                    $rootScope.product  = response.data;
                    $rootScope.full_rating  = response.data.calories;
                    $rootScope.food_name    = response.data.name;
                    $rootScope.category     = 'test';
                    $rootScope.fiber_rank = _.random(0,10);
                    $rootScope.sugar_rank = _.random(0,10);

                    $rootScope.alternate = response.alternate;
                    console.log($rootScope.product.name);
                    $rootScope.nutrient  = _.pairs(response.nutrient);
                    $rootScope.vitamin   = _.pairs(response.vitamin);
                });
            }

        }, function(error) {
            
            $ionicPopup.alert({
                title: 'Error!',
                template: 'Try scanning again.'
            });
        });
    }

    $scope.clear = function() {
        $scope.message = '';
    }
})

.controller('LoginCtrl', function($scope,$ionicViewService,lavoAPI,Auth,$location,$rootScope,$ionicHistory) {
        
   /* $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });*/

    $scope.avatar = 'assets/img/default/avatar.jpg';
    
    
    
    // If logged in end the method
    if(Auth.isLoggedIn()) {
        $location.path('/tab/scan/product');
        return;
    };


    // Form data
    $scope.formData = {};

    $scope.hasDetail =  false;

    // Get from cookie
    var tmp = Auth.getSession();

    if(tmp) {
        $scope.formData.email    = tmp.e;
        $scope.avatar = tmp.a;
        $scope.hasDetail =  true;
    }
    

    $scope.formData.email = 'this.zubair@gmail.com';
    $scope.formData.password = 'admin123';


    // Login form
    $scope.doLogin = function() {

        // API Call
        lavoAPI.login($scope.formData)
        .success(function(data) {
            
            if(data.token) {
                Auth.createSession(data.token,$scope.formData);
                console.log(data.token);
                lavoAPI.setToken(data.token,true);
                console.log(lavoAPI.getToken());

                $scope.message = 'Successfully logged In! Please Wait...';
                Auth.setStatus(true);

                // Build up profile
                lavoAPI.get('profile').success(function (response) {
        
                    Auth.setUser(response);
                    $location.path($rootScope.afterlogin);
                    $ionicViewService.clearHistory();
                    console.log('controllers.js: 172 #' + '');
                    return;
                })

            } else if (data.error) {
                // If not successful, bind errors to error variables
                $scope.errorName = data.title;
                $scope.message = data.description;
            }
        });
    };
})


.controller('AboutCtrl', function($scope,$ionicHistory) {
    $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });
})
.controller('SettingCtrl',function($scope,Auth,lavoAPI,$ionicHistory){

    $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });

    $scope.user = Auth.getUser();
    $scope.user_data = _.clone($scope.user);

    //Save Settigns
    $scope.update_setting = function() {

        lavoAPI.post('saveSetting',{notification:$scope.user_data.notification,privacy:$scope.user_data.privacy}).success(function (response) {
                
            if(response.success) {
                console.log('controllers.js: 194 #' + '');
                $scope.show_message(response.message,true);
                $scope.user = $scope.user_data;
            }
        });
    };

    //Delete account
    $scope.delete_acc_prompt = function(){
        
    };
});
